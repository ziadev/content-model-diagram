# Content Model Diagram Addon for Alfresco 7.x

This is an All-In-One (AIO) project uses Alfresco SDK 4.3.

Run ACS with the extension deployed with `./run.sh build_start` or `./run.bat build_start`.

If you just wish to build the AMP file you can `mvn clean package`

The run script offers the following tasks:

 * `build_start`. Build the whole project, recreate the ACS and Share docker images, start the dockerised environment composed by ACS, Share, ASS and
 PostgreSQL and tail the logs of all the containers.
 * `build_start_it_supported`. Build the whole project including dependencies required for IT execution, recreate the ACS and Share docker images, start the
 dockerised environment composed by ACS, Share, ASS and PostgreSQL and tail the logs of all the containers.
 * `start`. Start the dockerised environment without building the project and tail the logs of all the containers.
 * `stop`. Stop the dockerised environment.
 * `purge`. Stop the dockerised container and delete all the persistent data (docker volumes).
 * `tail`. Tail the logs of all the containers.
 * `reload_share`. Build the Share module, recreate the Share docker image and restart the Share container.
 * `reload_acs`. Build the ACS module, recreate the ACS docker image and restart the ACS container.
 * `build_test`. Build the whole project, recreate the ACS and Share docker images, start the dockerised environment, execute the integration tests from the
 `integration-tests` module and stop the environment.
 * `test`. Execute the integration tests (the environment must be already started).


## Usage

The webscript plugin is used to visualize Alfresco models as a content diagram.

### Endpoints

* [http://localhost:8080/alfresco/s/com/ziaconsulting/modelslist](http://localhost:8080/alfresco/s/com/ziaconsulting/modelslist) - Downloads Model diagram as an .svg content file

* [http://localhost:8080/alfresco/s/com/ziaconsulting/modeldiagram/{modelname}](http://localhost:8080/alfresco/s/com/ziaconsulting/modeldiagram/{modelname}) - Provide list of all models deployed in the repository

![Example Content Model Diagram](https://www.ziaconsulting.com/wp-content/webpc-passthru.php?src=https://www.ziaconsulting.com/wp-content/uploads/2018/07/custom-model-diagram.png&nocache=1)

## Deployment to existing Alfresco project

### Dependencies

In order for the content model diagrams to be generated, graphviz must be runnable from the repository runtime.

* [Graphviz](https://graphviz.org/)


### AMP Deployment

Instructions for installing AMPs for each of the deployment methods are available in the docs:

* [Manual Install](https://docs.alfresco.com/content-services/latest/install/zip/amp/)
* [Ansible Install](https://docs.alfresco.com/content-services/latest/install/ansible/#apply-your-own-modules-amp-or-jar)
* [Containerized Install](https://docs.alfresco.com/content-services/latest/install/containers/customize/)
