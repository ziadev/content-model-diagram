/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2014-2022 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */
package com.ziaconsulting.modeldiagram.webscripts;

import java.io.IOException;
import java.util.Collection;

import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.Comparator;
import java.util.Iterator;

public class GetModelsList extends AbstractWebScript {

    private DictionaryService dictionaryService;
    private static Log logger = LogFactory.getLog(GetModelsList.class);

	// generic method to sort TreeMap by value in ascending order
	static <K,V extends Comparable<? super V>> SortedSet<Map.Entry<K,V>> entriesSortedAscendingByValuesIgnoringCase(Map<K,V> map) {
        SortedSet<Map.Entry<K,V>> ascendingSortedEntries = new TreeSet<Map.Entry<K,V>>(
            new Comparator<Map.Entry<K,V>>() {
                @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
                		if (e1.getValue() instanceof String) {
                			int res = ((String) e1.getValue()).compareToIgnoreCase((String) e2.getValue());  // reverse order of comparison: e2 compare e1 is descending order.  e1 compare e2 is ascending order
                			return res != 0 ? res : 1; // Special fix to preserve items with equal values
                		} else {
                			int res = e1.getValue().compareTo(e2.getValue());  // reverse order of comparison: e2 compare e1 is descending order.  e1 compare e2 is ascending order
                			return res != 0 ? res : 1; // Special fix to preserve items with equal values

                		}
                }
            }
        );
        ascendingSortedEntries.addAll(map.entrySet());
        return ascendingSortedEntries;
    }

	public void setDictionaryService(DictionaryService dictionaryService)
    {
        this.dictionaryService = dictionaryService;
    }

	@Override
	public void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {
		logger.info("Starting GetModelsList");
		String modelsList = "";
		Map<String, String> keysAndValues = new TreeMap<String, String>();
		Collection<QName> allModelQNames = dictionaryService.getAllModels();
		for (QName modelQName : allModelQNames) {
			keysAndValues.put(modelQName.getPrefixString(), modelQName.getLocalName());
		}
		SortedSet <Entry<String, String>> set = entriesSortedAscendingByValuesIgnoringCase(keysAndValues);
		Iterator<Entry<String, String>>iter = set.iterator();
		while (iter.hasNext()) {
			Entry<String, String> entry = iter.next();
			modelsList += entry.getKey() + "\n";
		}

		res.getWriter().write(modelsList);
	}

}
