/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2014-2022 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */
package com.ziaconsulting.modeldiagram.webscripts;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.dictionary.AssociationDefinition;
import org.alfresco.service.cmr.dictionary.ClassDefinition;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.dictionary.ModelDefinition;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;
import net.sourceforge.plantuml.core.DiagramDescription;

import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.web.scripts.content.StreamContent;

public class GetModelDiagram extends StreamContent {

    private static Log logger = LogFactory.getLog(GetModelDiagram.class);

    private static final String ALFRESCO_GREEN = "#8FC549";
	private static final String ALFRESCO_ORANGE = "#FD9827";
	private static final String AGGREGATION_RELATIONSHIP = " o-[bold]- ";
	private static final String INHERITANCE_RELATIONSHIP = " <|-[bold]- ";
	private static final String PLANT_UML_SPOT_ASPECT_ALFRESCO_GREEN = " << (A, " + ALFRESCO_GREEN + ") >> ";
	private static final String PLANT_UML_SPOT_TYPE_ALFRESCO_ORANGE = " << (T, " + ALFRESCO_ORANGE + ") >> ";
	private DictionaryService dictionaryService;
	private static final String FILE_NAME_PREFIX = "ModelDiagram";
	private static final String PROPERTY_NAME_WIDTH = "25";

    public void setDictionaryService(DictionaryService dictionaryService)
    {
        this.dictionaryService = dictionaryService;
    }

	@Override
	public void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {
		String modelName = req.getServiceMatch().getTemplateVars().get("modelname");
		logger.info("Starting GetModelDiagram for modelname: " + modelName);
		List<String> modelNames = Arrays.asList(modelName.split("&"));
		for (String mName : modelNames) {
			logger.debug("processing model name: " + mName);
		}

		BufferedWriter writer = null;
		try {
			setContentTypeAndCaching(res);

			File file = TempFileProvider.createTempFile("img", ".svg");

			//System.out.println(file.getCanonicalPath());

			//String sampleSVG = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"100\" height=\"100%\"><circle cx=\"50\" cy=\"50\" r=\"30\" fill=\"red\"/></svg>";

			String samplePlantUMLString = createPlantUMLModel(modelNames);

			SourceStringReader reader = new SourceStringReader(samplePlantUMLString);
			final ByteArrayOutputStream os = new ByteArrayOutputStream();
			// Write the first image to "os"
			DiagramDescription desc = reader.outputImage(os, new FileFormatOption(FileFormat.SVG));
			os.close();

			// The XML is stored into svg
			final String svg = new String(os.toByteArray(), Charset.forName("UTF-8"));

			writer = new BufferedWriter(new FileWriter(file));
			writer.write(svg);
			writer.flush();

			// stream the file back to client (with the attachFileName as the downloaded file name)
			streamContent(req, res, file);
        } catch (Exception e) {
			throw new WebScriptException(Status.STATUS_BAD_REQUEST, "Could not output model: " + e.getMessage(),
					e);
		} finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
	}

	private String createPlantUMLModel(List<String> modelNames) {
		List<String> modelPrefixes = new ArrayList<String>();
		for (String mName : modelNames) {
			modelPrefixes.add(mName.split(":")[0]);
		}
		// PlantUML "header"
		String plantUMLHeader = "@startuml\n";
		plantUMLHeader += "title Content Model: " + modelNames + "\n";
		plantUMLHeader += "skinparam shadowing false\n";
		plantUMLHeader += "skinparam linetype ortho\n";
		plantUMLHeader += "skinparam class {\n";
		plantUMLHeader += "    AttributeFontSize 14\n";
		plantUMLHeader += "    AttributeFontName Courier\n";
		plantUMLHeader += "}\n";
		String plantUMLFooter = "@enduml\n";

		String modelPlantUML = plantUMLHeader;

		// sample PlantUML
		String samplePlantUMLString = plantUMLHeader;
		samplePlantUMLString += "Package Types {\n";
		samplePlantUMLString += "class c1{\n";
		samplePlantUMLString += "  property1 | d:text\n";
		samplePlantUMLString += "  property2 | d:int\n";
		samplePlantUMLString += "}\n";
		samplePlantUMLString += "Package Aspects {\n";
		samplePlantUMLString += "  class aspect1 {\n";
		samplePlantUMLString += "  property1 | d:text\n";
		samplePlantUMLString += "  property2 | d:int\n";
		samplePlantUMLString += "}\n";
		samplePlantUMLString += "}\n";
		samplePlantUMLString += "c1 o-- aspect1\n";
		samplePlantUMLString += plantUMLFooter;

		// query data dictionary for content model

		Map<String, List<QName>> modelAspectQNames = new HashMap<String, List<QName>>();
		Map<String, List<QName>> modelTypeQNames = new HashMap<String, List<QName>>();

		for (String modelName : modelNames) {
			List<QName> aspectQNames = new ArrayList<QName>();
			List<QName> typeQNames = new ArrayList<QName>();
			String modelPrefix = modelName.split(":")[0];

			QName diagrammedModelQName = null;

			Collection<QName> allModelQNames = dictionaryService.getAllModels();
			for (QName modelQName : allModelQNames) {
				String modelPrefixedQName = modelQName.getPrefixString();
				if (modelPrefixedQName.equals(modelName)) {
					diagrammedModelQName = modelQName;
				}
			}
			if (null != diagrammedModelQName) {
				logger.debug("Getting model: " + diagrammedModelQName);
				ModelDefinition modelDef = dictionaryService.getModel(diagrammedModelQName);
				if (modelDef != null)
				{
					typeQNames.addAll(dictionaryService.getTypes(diagrammedModelQName));
					aspectQNames.addAll(dictionaryService.getAspects(diagrammedModelQName));
				}
			}

			modelAspectQNames.put(modelName, aspectQNames);
			modelTypeQNames.put(modelName, typeQNames);

			modelPlantUML = layoutEntity("\"" + modelName + " Types\"", typeQNames, modelPlantUML);
			modelPlantUML = layoutEntity("\"" + modelName + " Aspects\"", aspectQNames, modelPlantUML);
			modelPlantUML = connectTypesAndAspects(typeQNames, modelPrefix, modelPlantUML);
		}
		modelPlantUML = connectChildrenAndParents(modelPrefixes, modelTypeQNames, PLANT_UML_SPOT_TYPE_ALFRESCO_ORANGE, ALFRESCO_ORANGE, modelPlantUML);
		modelPlantUML = connectChildrenAndParents(modelPrefixes, modelAspectQNames, PLANT_UML_SPOT_ASPECT_ALFRESCO_GREEN, ALFRESCO_ORANGE, modelPlantUML);

		modelPlantUML += plantUMLFooter;
        return modelPlantUML;
		//return samplePlantUMLString;
	}

	private String connectTypesAndAspects(List<QName> typeQNames, String modelPrefix, String modelPlantUML) {
        for (QName qname : typeQNames) {
        		String typePrefixedName = qname.toPrefixString();
        		ClassDefinition classDef = dictionaryService.getClass(qname);
        		for (QName aspectName : classDef.getDefaultAspectNames()) {
        			String aspectEntry = aspectName.toPrefixString();
        			if (aspectEntry.startsWith(modelPrefix)) {
        				modelPlantUML += "\"" + typePrefixedName + "\"" + AGGREGATION_RELATIONSHIP + "\"" + aspectEntry + "\" " + ALFRESCO_GREEN + "\n";
        			}
        		}
        }
        return modelPlantUML;
	}

	private String connectChildrenAndParents(List <String> modelPrefixes, Map<String, List<QName>> modelClassQNames, String spotString, String lineColor, String modelPlantUML) {
        for (Map.Entry<String, List<QName>> entry : modelClassQNames.entrySet()) {
        		List<QName> classQNames = entry.getValue();
        		for (QName qname : classQNames) {
        			String typePrefixedName = qname.toPrefixString();
        			ClassDefinition classDef = dictionaryService.getClass(qname);
        			QName parentQName = classDef.getParentName();
        			if (parentQName != null) {
        				String parentName = parentQName.toPrefixString();
        				// if the parent qname doesn't exist in our list of typeQNames, add it outside of packages
        				if (!classQNames.contains(parentQName)) {
        					// before including a separate class def, see if the parent is in another namespace we are processing
        					String parentNamespacePrefix = parentName.split(":")[0];
        					if (!modelPrefixes.contains(parentNamespacePrefix)) {
        						modelPlantUML += "class \"" + parentName + "\""+ spotString + "\n";
        					}
        				}
        				modelPlantUML += "\"" + parentName + "\"" + INHERITANCE_RELATIONSHIP + "\"" + typePrefixedName + "\" " + lineColor + "\n";
        			}
        		}
        }
        return modelPlantUML;
	}

	private String layoutEntity(String entityName, List<QName> qNames, String modelPlantUML) {
		String typesPlantUMLHeader = "Package " + entityName + "{\n";
		modelPlantUML +=	 typesPlantUMLHeader;

		// separate the classes that have parents and those that don't
		List<QName> qNamesWithParents = new ArrayList<QName>();
		List<QName> qNamesWithoutParents = new ArrayList<QName>();
        for (QName qname : qNames) {
			ClassDefinition classDef = dictionaryService.getClass(qname);
			QName parentQName = classDef.getParentName();
			if (parentQName != null) {
				qNamesWithParents.add(qname);
			} else {
				qNamesWithoutParents.add(qname);
			}
        }

        // get the class definitions and the properties and associations
		// types
        modelPlantUML = layoutSection(qNamesWithParents, modelPlantUML);
        if (!qNamesWithoutParents.isEmpty()) {
			modelPlantUML += "together {\n";
        		modelPlantUML = layoutSection(qNamesWithoutParents, modelPlantUML);
    			modelPlantUML += "}\n";
        }
        // package closing brace
        modelPlantUML += "}\n";
        return modelPlantUML;
	}

	private String layoutSection(List<QName> qNames, String modelPlantUML) {
		for (QName qname : qNames) {
        		String typePrefixedName = qname.toPrefixString();
        		ClassDefinition classDef = dictionaryService.getClass(qname);

        		// Class type spot
        		String plantUMLSpotChar = PLANT_UML_SPOT_TYPE_ALFRESCO_ORANGE;
        		if (classDef.isAspect()) {
        			plantUMLSpotChar = PLANT_UML_SPOT_ASPECT_ALFRESCO_GREEN;
        		}

        		// class name
        		modelPlantUML += "class \"" + typePrefixedName + "\""+ plantUMLSpotChar + "{" + "\n";

        		// properties (represented as class member variables)
        		for (PropertyDefinition propdef : classDef.getProperties().values()) {
        			String propEntry = String.format("%1$-" + PROPERTY_NAME_WIDTH + "s | %2$10s", propdef.getName().toPrefixString(),
        					propdef.getDataType().getName().toPrefixString())  + "\n";
        			modelPlantUML += propEntry;
        		}

        		// mandatory aspects (represented as class methods)
        		for (QName aspectName : classDef.getDefaultAspectNames()) {
        			String aspectEntry = "{method}" + aspectName.toPrefixString() + "\n";
        			modelPlantUML += aspectEntry;
        		}

        		modelPlantUML += "}\n";
        		//assocdef.put(qname, classDef.getAssociations().values());
        }
		return modelPlantUML;
	}

	// Set content type and caching on webscript response object
	//
	private void setContentTypeAndCaching(WebScriptResponse res) {
		res.setContentType(MimetypeMap.MIMETYPE_IMAGE_SVG);
		Cache cache = new Cache();
		cache.setNeverCache(true);
		cache.setMustRevalidate(true);
		cache.setMaxAge(0L);
		res.setCache(cache);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		String formattedDate = sdf.format(new Date());
		String attachFileName = FILE_NAME_PREFIX + "-" + formattedDate;
		String headerValue = "attachment; filename=\"" + attachFileName + ".svg\"";

		// set header based on filename
		res.setHeader("Content-Disposition", headerValue);
	}

}
